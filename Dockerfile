FROM python:3.8

COPY requirements.txt /usr/src/app/requirements.txt
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt


COPY lakefs_key/id_rsa /root/.ssh/id_rsa
COPY lakefs_key/id_rsa.pub /root/.ssh/id_rsa.pub
COPY data/file_1_v1.txt /usr/src/app/local/data/file_1_v1.txt
COPY data/file_1_v2.txt /usr/src/app/local/data/file_1_v2.txt
COPY lakefs_setup.sh /usr/src/app/lakefs_setup.sh

RUN chmod 600 /root/.ssh/id_rsa && \
    chmod 644 /root/.ssh/id_rsa.pub && \
    chmod +x /usr/src/app/lakefs_setup.sh


CMD ["/usr/src/app/lakefs_setup.sh"]