# Проверка наличия lakectl и установка, если он не установлен
if ! command -v lakectl &> /dev/null
then
    echo "lakectl is not installed. Installing..."
    curl -sL https://github.com/treeverse/lakeFS/releases/latest/download/lakectl_$(uname -s)_amd64.tar.gz | tar xz -C /usr/local/bin
    echo "lakectl installed successfully."
fi

# Создание репозитория в lakefs
lakectl repo create myrepo

# Проверка успешного создания репозитория
if [ $? -eq 0 ]; then
    echo "Repository 'myrepo' created successfully."
else
    echo "Error creating repository 'myrepo'. Exiting."
    exit 1
fi

# Список репозиториев в lakefs
echo "List of repositories in lakefs:"
lakectl repo ls

# Загрузка файлов в lakefs
lakectl fs upload /usr/src/app/local/data/file_1_v1.txt lakefs://myrepo/path/file_1_v1.txt
lakectl fs upload /usr/src/app/local/data/file_1_v2.txt lakefs://myrepo/path/file_1_v2.txt

# Загрузка файлов в S3 bucket
echo "Copying files to S3 bucket..."
aws s3 cp "lakefs://myrepo/path/file_1_v1.txt" "s3://your-s3-bucket/path/file_1_v1.txt"
aws s3 cp "lakefs://myrepo/path/file_1_v2.txt" "s3://your-s3-bucket/path/file_1_v2.txt"

echo "Files copied successfully to S3 bucket."

pip install apache-airflow

airflow initdb

airflow scheduler &

airflow webserver &

sleep 30

airflow dags unpause lakefs_pipeline

airflow trigger_dag lakefs_pipeline

airflow webserver -k shutdown
airflow scheduler -k shutdown