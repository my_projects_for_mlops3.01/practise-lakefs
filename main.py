from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.contrib.hooks.ssh_hook import SSHHook
from airflow.contrib.operators.ssh_operator import SSHOperator

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2024, 5, 11),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
}

dag = DAG('lakefs_pipeline', default_args=default_args, schedule_interval=None)

def upload_to_lakefs(version, **kwargs):
    ssh_hook = SSHHook(ssh_conn_id='lakefs_ssh_conn')
    command = f'lakectl fs upload local/data/file_1_v{version}.txt lakefs://myrepo/path/file_1_v{version}.txt'
    ssh_operator = SSHOperator(
        task_id=f'upload_file_v{version}_to_lakefs',
        ssh_hook=ssh_hook,
        command=command,
        dag=dag,
    )
    return ssh_operator.execute(context=kwargs)

def process_file(version, **kwargs):
    print(f'Processing file version {version}')

upload_v1 = PythonOperator(
    task_id='upload_file_v1',
    python_callable=upload_to_lakefs,
    op_args=[1],
    provide_context=True,
    dag=dag,
)

upload_v2 = PythonOperator(
    task_id='upload_file_v2',
    python_callable=upload_to_lakefs,
    op_args=[2],
    provide_context=True,
    dag=dag,
)

process_v1 = PythonOperator(
    task_id='process_file_v1',
    python_callable=process_file,
    op_args=[1],
    provide_context=True,
    dag=dag,
)

process_v2 = PythonOperator(
    task_id='process_file_v2',
    python_callable=process_file,
    op_args=[2],
    provide_context=True,
    dag=dag,
)

upload_v1 >> upload_v2 >> process_v1 >> process_v2